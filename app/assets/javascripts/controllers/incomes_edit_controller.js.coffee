wherzmamoneyApp.controller "IncomesEditCtrl", ["$scope", "$routeParams", "$window", "Income", ($scope, $routeParams, $window, Income) ->

  Income.get id: $routeParams["id"], (income, getResponseHeaders) ->
    $scope.income = income

  # Form on-submit event
  $scope.submit = ->
    $scope.income.$save ((income, postResponseHeaders) ->
      alert "Income successfully saved"
      $window.history.back()
    ), (error, postResponseHeaders) ->
      alert error
      console.log postResponseHeaders

]