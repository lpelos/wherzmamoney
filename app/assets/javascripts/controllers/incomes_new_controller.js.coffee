wherzmamoneyApp.controller "IncomesNewCtrl", ["$scope", "Income", "MyDate", ($scope, Income, MyDate) ->

  $scope.income = new Income
    date: new MyDate().today()

  # Form on-submit event
  $scope.submit = ->
    $scope.income.$save ((income, postResponseHeaders) ->
      alert "Income successfully saved"
      $scope.income = new Income
        date: new MyDate().today()
    ), (error, postResponseHeaders) ->
      alert error
      console.log postResponseHeaders

]