wherzmamoneyApp.controller "OutgoesEditCtrl", ["$scope", "$routeParams", "$window", "Outgo", ($scope, $routeParams, $window, Outgo) ->

  # Tags
  $tagsEl  = jQuery("#outgo_tags")
  userTags = $tagsEl.data("userTags")

  # Get Outgo data from API
  Outgo.get id: $routeParams["id"], (outgo, getResponseHeaders) ->
    $scope.outgo = outgo

    $tagsEl.select2
      tags: userTags

    $tagsEl.select2 "val", outgo.tags

  # Form on-submit event
  $scope.submit = ->
    $scope.outgo.tags = $tagsEl.select2("val")

    $scope.outgo.$save ((outgo, postResponseHeaders) ->
      alert "Outgo successfully saved"
      $window.history.back()
    ), (error, postResponseHeaders) ->
      alert error
      console.log postResponseHeaders

]