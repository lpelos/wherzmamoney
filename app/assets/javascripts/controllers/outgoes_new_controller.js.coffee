wherzmamoneyApp.controller "OutgoesNewCtrl", ["$scope", "Outgo", "MyDate", ($scope, Outgo, MyDate) ->

  # Helpers
  clearTags = ->
    $tagsEl.select2 "val", []

  getTagsVal = ->
    $tagsEl.select2("val")

  # Tags
  $tagsEl  = jQuery("#outgo_tags")
  userTags = $tagsEl.data("userTags")
  $tagsEl.select2(tags: userTags)
  clearTags()

  # Creates a new Outgo
  $scope.outgo = new Outgo
    date: new MyDate().today()

  # Form on-submit event
  $scope.submit = ->
    $scope.outgo.tags = getTagsVal()

    $scope.outgo.$save ((outgo, postResponseHeaders) ->
      alert "Outgo successfully saved"

      $scope.outgo = new Outgo
        date: new MyDate().today()

      clearTags()
    ), (error, postResponseHeaders) ->
      alert error
      console.log postResponseHeaders

]