wherzmamoneyApp.controller "TransactionsIndexCtrl", ["$scope", "$routeParams", "$location", "$modal", "Transaction", ($scope, $routeParams, $location, $modal, Transaction) ->

  # Helpers
  isObjEmpty = (obj) ->
    Object.keys(obj).length == 0

  # Methods
  $scope.getItemUrl = (transaction, search = "") ->
    type = $scope.getType(transaction)
    "#/#{type}/#{transaction.id}" + search

  $scope.getListUrl = (type = "transactions", year, month, search = {}) ->
    url = "#/extracts/#{type}"
    url += "/#{year}"  if year?
    url += "/#{month}" if month?
    url += "?#{$scope.objToQueryString(search)}" unless isObjEmpty(search)
    url

  $scope.getType = (transaction) ->
    type =  if transaction.category?
      type = "outgoes"
    else if transaction.source?
      type = "incomes"

  $scope.openMonthModal = ->
    $modal.open
      templateUrl: "transactions/modals/by-month"
      controller:  "TransactionsByMonthModalCtrl"
      windowClass: "transactions-by-month-modal"
      resolve:
        type: ->
          $scope.type

  $scope.objToQueryString = (obj) ->
    str = []
    for k, v of obj
      str.push(if angular.isObject(v) then $scope.objToQueryString(v, k) else (k) + "=" + encodeURIComponent(v))
    str.join("&")

  $scope.openYearModal = ->
    $modal.open
      templateUrl: "transactions/modals/by-year"
      controller:  "TransactionsByYearModalCtrl"
      windowClass: "transactions-by-year-modal"

  $scope.paramsToModels = (params) ->
    for k, v of params
      $scope[k] = v

  # Initialize Params Modals
  $scope.paramsToModels $routeParams
  $scope.type ||= "transactions"

  $scope.title = ""

  $scope.totalIncome = 0
  $scope.totalOutgo  = 0
  $scope.totalValue  = 0

  $scope.showTotals = !isObjEmpty($routeParams) #corrigir para mostrar sempre, exceto em recents

  # Tabs urls
  $scope.tabs = {
    incomes: $scope.getListUrl("incomes", $scope.year, $scope.month, $location.search()),
    outgoes: $scope.getListUrl("outgoes", $scope.year, $scope.month, $location.search()),
    transactions: $scope.getListUrl("transactions", $scope.year, $scope.month, $location.search())
  }

  # Get Transactions array
  Transaction.query $routeParams, (response, getResponseHeaders) ->
    if response.meta.income? && response.meta.outgo?
      $scope.totalIncome = response.meta.income
      $scope.totalOutgo  = response.meta.outgo
      $scope.totalValue  = $scope.totalIncome - $scope.totalOutgo

    $scope.title = response.meta.title
    $scope.transactions = response.transactions
  , (error, getResponseHeaders) ->
    alert error

]