wherzmamoneyApp.controller "TransactionsByMonthModalCtrl", ["$scope", "$modalInstance", "Months", "type", ($scope, $modalInstance, Months, type) ->

  $scope.months = Months
  $scope.year   = parseInt(new Date().getFullYear())
  $scope.type   = type || "transactions"

  $scope.close = ->
    $modalInstance.dismiss 'cancel'

  $scope.nextYear = ->
    $scope.year += 1

  $scope.prevYear = ->
    $scope.year -= 1

]