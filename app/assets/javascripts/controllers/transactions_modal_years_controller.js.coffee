wherzmamoneyApp.controller "TransactionsByYearModalCtrl", ["$scope", "$modalInstance", ($scope, $modalInstance) ->

  $scope.year = parseInt(new Date().getFullYear())

  $scope.close = ->
    $modalInstance.dismiss 'cancel'

  $scope.nextYear = ->
    $scope.year += 1

  $scope.prevYear = ->
    $scope.year -= 1

]