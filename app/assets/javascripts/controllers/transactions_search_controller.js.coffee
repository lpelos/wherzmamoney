wherzmamoneyApp.controller "TransactionsSearchCtrl", ["$scope", "$location", ($scope, $location) ->

  # Helpers
  capitalize = (string) ->
    string.charAt(0).toUpperCase() + string.slice(1)

  isObjEmpty = (obj) ->
    Object.keys(obj).length == 0

  # Initialize Models
  $scope.showDate = false
  $scope.showValue = false
  $scope.showSource = false
  $scope.showCategory = false
  $scope.showDescription = false

  # Tags
  $scope.$categoriesEl = jQuery("#search_categories")
  $scope.$sourcesEl    = jQuery("#search_sources")
  $scope.$tagsEl       = jQuery("#search_tags")

  $scope.$categoriesEl.select2(placeholder: "Categories")
  $scope.$sourcesEl.select2(placeholder: "Sources")
  $scope.$tagsEl.select2(placeholder: "Tags")

  $scope.submit = (search = {}) ->
    params = {}

    if $scope.showDate
      params["date_from"] = search.dateFrom if search.dateFrom
      params["date_to"]   = search.dateTo if search.dateTo

    if $scope.showValue
      params["value_from"] = search.valueFrom if search.valueFrom
      params["value_to"]   = search.valueTo if search.valueTo

    if $scope.showSource
      params["sources[]"] = $scope.$sourcesEl.select2("val")

    if $scope.showCategory
      params["categories[]"] = $scope.$categoriesEl.select2("val")

    if $scope.showTags
      params["tags[]"] = $scope.$tagsEl.select2("val")

    $location.search params
    $location.path "/extracts/transactions/"

  $scope.toggle = (filter) ->
    modelName = "show" + capitalize(filter)
    $scope[modelName] = !$scope[modelName]

  jQuery ($) ->
    # Add step to currency-fields
    $(".currency-field").attr("step", "0.01")

]