wherzmamoneyApp.controller "TransactionsYearExtractCtrl", ["$scope", "$routeParams", "$modal", "YearExtract", "Months", ($scope, $routeParams, $modal, YearExtract, Months) ->
  TITLE = "Extract"

  # Methods
  $scope.getBalance = (month) ->
    month.values.income - month.values.outgo

  $scope.getItemUrl = (month) ->
    "#/extracts/transactions/#{$scope.year}/" + month.month

  $scope.getMonthName = (month) ->
    month_index = parseInt(month.month)
    $scope.month_names[month_index - 1].name

  $scope.getTitle = (year) ->
    TITLE + " - #{year}"

  $scope.openMonthModal = ->
    $modal.open
      templateUrl: "transactions/modals/by-month"
      controller:  "TransactionsByMonthModalCtrl"
      windowClass: "transactions-by-month-modal"
      resolve:
        type: ->
          $scope.type

  $scope.openYearModal = ->
    $modal.open
      templateUrl: "transactions/modals/by-year"
      controller:  "TransactionsByYearModalCtrl"
      windowClass: "transactions-by-year-modal"

  # Initialize Params Modals
  $scope.month_names = Months
  $scope.year = $routeParams['year']  || null

  $scope.title = ""

  $scope.totalIncome = 0
  $scope.totalOutgo  = 0
  $scope.totalValue  = 0

  $scope.title = $scope.getTitle($scope.year)

  # Get Transactions array
  YearExtract.query
    year:  $scope.year
  , (response, getResponseHeaders) ->
    $scope.totalIncome = response.meta.income
    $scope.totalOutgo  = response.meta.outgo
    $scope.totalValue  = $scope.totalIncome - $scope.totalOutgo
    $scope.title = response.meta.title
    $scope.valuesByMonth = response.transactions

  , (error, getResponseHeaders) ->
    alert error

]