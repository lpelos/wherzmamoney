wherzmamoneyApp.controller "WelcomeCtrl", ["$scope", "$location", ($scope, $location) ->

  location = $location.path().replace("/", "")
  $scope.location = location || "signup"

  $scope.reditectUrl = $location.search().url || ""

  $scope.isLoginEmailFormVisible = false
  $scope.showLoginEmailForm = ->
    $scope.isLoginEmailFormVisible = true

  $scope.isSignUpEmailFormVisible = false
  $scope.showSignUpEmailForm = ->
    $scope.isSignUpEmailFormVisible = true
]