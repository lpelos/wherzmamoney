wherzmamoneyApp.filter "valueArrow", ->

  (input, isPositive = true) ->
    if isPositive
      input = "<span class='green icomoon'>&#xe724;</span>&nbsp;" + input
    else
      input = "<span class='red icomoon'>&#xe728;</span>&nbsp;" + input

    input