wherzmamoneyApp.config ["$routeProvider", ($routeProvider) ->

  # Root Route
  $routeProvider.when("/",
    redirectTo: "outgoes/new"

  # 404
  ).when("/404"
    templateUrl: "404.html"

  # Import / Export
  ).when("/import-export",
    templateUrl: "import_export"

  # Incomes
  ).when("/incomes/:id/edit",
    templateUrl: "incomes/edit"
    controller: "IncomesEditCtrl"

  ).when("/incomes/new",
    templateUrl: "incomes/new"
    controller: "IncomesNewCtrl"

  # Outgoes
  ).when("/outgoes/new",
    templateUrl: "outgoes/new"
    controller: "OutgoesNewCtrl"

  ).when("/outgoes/:id/edit",
    templateUrl: "outgoes/edit"
    controller: "OutgoesEditCtrl"

  # Transactions
  ).when("/transactions/search",
    templateUrl: "transactions/search"
    controller: "TransactionsSearchCtrl"

  # Welcome Screen
  ).when("/signup",
    templateUrl: "welcome"
    controller: "WelcomeCtrl"

  ).when("/login",
    templateUrl: "welcome"
    controller: "WelcomeCtrl"

  ).when("/watzit",
    templateUrl: "welcome"
    controller: "WelcomeCtrl"

  # Extracts
  ).when("/extracts",
    redirectTo: "/extracts/transactions"

  ).when("/extracts/:type",
    templateUrl: "transactions"
    controller: "TransactionsIndexCtrl"

  ).when("/extracts/:type/:year",
    templateUrl: "transactions/by_year"
    controller: "TransactionsYearExtractCtrl"

  ).when("/extracts/:type/:year/:month",
    templateUrl: "transactions"
    controller: "TransactionsIndexCtrl"

  ).when("/extracts/:type/?:filters",
    templateUrl: "transactions"
    controller: "TransactionsIndexCtrl"

  ).otherwise
    redirectTo: "404"

]