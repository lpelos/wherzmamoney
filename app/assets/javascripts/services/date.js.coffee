wherzmamoneyApp.factory "MyDate", [ ->

  class MyDate
    today: ->
      today = new Date()
      day   = today.getDate()
      month = today.getMonth() + 1
      year  = today.getFullYear()

      day   = '0' + day if day < 10
      month = '0' + month if month < 10

      year + '-' + month + '-' + day

]