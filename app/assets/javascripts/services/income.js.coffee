wherzmamoneyApp.factory "Income", ['Resource', ($resource) ->

  $resource "/incomes/:id", id: "@id"

]