wherzmamoneyApp.factory "Months", [ ->

  [
    { index:  1, name: "January",   shortName: "Jan" },
    { index:  2, name: "Febuary",   shortName: "Feb" },
    { index:  3, name: "March",     shortName: "Mar" },
    { index:  4, name: "April",     shortName: "Apr" },
    { index:  5, name: "May",       shortName: "May" },
    { index:  6, name: "June",      shortName: "Jun" },
    { index:  7, name: "July",      shortName: "Jul" },
    { index:  8, name: "August",    shortName: "Aug" },
    { index:  9, name: "September", shortName: "Sep" },
    { index: 10, name: "October",   shortName: "Oct" },
    { index: 11, name: "November",  shortName: "Nov" },
    { index: 12, name: "December",  shortName: "Dec" },
  ]

]