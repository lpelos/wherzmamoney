wherzmamoneyApp.factory "Outgo", ['Resource', ($resource) ->

  $resource "/outgoes/:id", id: "@id"

]