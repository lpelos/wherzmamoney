wherzmamoneyApp.factory 'Resource', ['$resource', ($resource) ->

  (url, params, methods) ->
    defaults =
      update:
        method: 'patch'
        isArray: false
      create:
        method: 'post'

    methods  = angular.extend(defaults, methods)
    resource = $resource(url, params, methods)

    resource.prototype.$save = (successCalback, errorCallback) ->
      if not this.id
        this.$create successCalback, errorCallback
      else
        this.$update successCalback, errorCallback

    resource
]