wherzmamoneyApp.factory "Transaction", ['$resource', ($resource) ->

  $resource "/transactions.json", {},
    query:
      method: "GET"
      isArray: false
]