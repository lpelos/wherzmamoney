wherzmamoneyApp.factory "YearExtract", ['$resource', ($resource) ->

  $resource "/transactions/by_year/:year.json",
    # Params
    year:  "@year"
  ,
    # Methods
    query:
      method: "GET"
      isArray: false
]