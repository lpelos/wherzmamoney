jQuery ($) ->
  $(document).foundation()

  # not working
  # must make a Directive to execute this piece of code after
  # angular templates are completly rendered
  #
  # resizeMainSection = ->
  #   $(".main-section").css({ minHeight: window.innerHeight })

  # $(window).on "resize", resizeMainSection
  # resizeMainSection()

window.wherzmamoneyApp = angular.module('wherzmamoneyApp', ["ngResource", "ngRoute", "ngSanitize", "mm.foundation"])

  # Redirects user to login page if unauthorized
  .config ['$httpProvider', ($httpProvider) ->
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')

    interceptor = ['$location', '$rootScope', '$q', ($location, $rootScope, $q) ->
      success = (response) ->
        response

      error = (response) ->
        if response.status == 401
          $rootScope.$broadcast 'event:unauthorized'
          atempt = $location.path()

          if !!atempt && atempt != "/"
            $location.path('/login').search 'url', atempt
          else
            $location.path('/signup')

          response

        $q.reject response

      (promise) ->
        promise.then(success, error)
    ]

    $httpProvider.responseInterceptors.push interceptor
  ]

# .run ($rootScope, $window) ->
  # $rootScope.defaultForwardTransition = 'animate-slide-left'
  # $rootScope.defaultBackwardTransition = 'animate-slide-right'

  # $rootScope.backTransition = null
  # $rootScope.transitionClass = null
  # $rootScope.stateHistory = []

  # # Back transitions stores the inverse of a transition
  # $rootScope.backTransitions =
  #   "animate-slide-left"  : "animate-slide-right"
  #   "animate-slide-right" : "animate-slide-left"
  #   "animate-slide-bottom": "animate-slide-top"
  #   "animate-slide-top"   : "animate-slide-bottom"

  # # Call this function to set a transition when changing pages
  # $rootScope.setTransition = (animation) ->
  #   $rootScope.transitionClass = animation

  # $rootScope.$on "$locationChangeSuccess", (event, next, current) ->
  #   # Back transition is the opposite of the current transition
  #   $rootScope.backTransition = $rootScope.backTransitions[$rootScope.transitionClass]

  #   # url slug : shortening the url to stuff that follows after "#"
  #   next = next.slice(next.lastIndexOf('#') + 1, next.length)
  #   $rootScope.stateHistory.push next

  #   # We need to reset the transition after the animations
  #   # That's the solution for now, deal with it
  #   $window.setTimeout (->
  #     $rootScope.setTransition null;
  #   ), 300

  # $rootScope.$on("$locationChangeStart", (event, next, current) ->
  #   next = next.slice(next.lastIndexOf('#') + 1, next.length)

  #   # Is it a history back?
  #   if  next == $rootScope.stateHistory[$rootScope.stateHistory.length - 2]
  #     $rootScope.stateHistory.pop()

  #     # Didn't choose a transition?
  #     if !$rootScope.transitionClass
  #       $rootScope.setTransition($rootScope.backTransition || $rootScope.defaultBackwardTransition)
  #       true;

  #   $rootScope.setTransition($rootScope.transitionClass || $rootScope.defaultForwardTransition)
