class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  before_filter :require_authentication
  before_filter :set_locale

  private

  def accept_language
    request.env["HTTP_ACCEPT_LANGUAGE"].split(/[\,\;]/).map(&:to_sym)
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
    # @current_user ||= FacebookUser.find(session[:fuser_id]) if session[:fuser_id]
  end

  def default_url_options(options = {})
    { locale: I18n.locale }
  end

  def set_locale
    I18n.locale = params[:locale] || (accept_language & I18n.available_locales).first || :en
  end

  def require_authentication
    render :nothing => true, :status => 401 if session[:user_id].nil?
  end

end
