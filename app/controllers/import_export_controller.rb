class ImportExportController < ApplicationController

  def create
    resource_name = params[:import][:resource]
    import_file(resource_name, params[:file])
    redirect_to "/#/import-export", :notice => "#{resource_name} imported"
  end

  def index
    @resources = ["Categories", "Incomes", "Outgoes", "Sources", "Tags"]
    render :layout => nil
  end

  def show
    export_to_file
  end

  private

  def export_to_file
  end

  def import_file(resource, file)
    case resource.to_s.downcase
      when "categories"
        Category.import file, current_user
      when "incomes"
        Income.import file, current_user
      when "outgoes"
        Outgo.import file, current_user
      when "sources"
        Source.import file, current_user
      when "tags"
        Tag.import file, current_user
    end
  end

end