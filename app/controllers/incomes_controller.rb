class IncomesController < ApplicationController
  respond_to :js, :only => [:create, :show, :update]

  def by_month
    date = params[:date] || Date.today
    @incomes = Income.by_month(date).order("date DESC, created_at DESC")

    render :action => :index, :layout => nil
  end

  def create
    @income = current_user.incomes.build(params[:income])

    if @income.save
      render :json => @income, :status => 201
    else
      render :json => "this should be an error message", :status => 500
    end
  end

  def csv
    @incomes_csv = current_user.incomes.scoped.to_csv

    send_data(
      @incomes_csv,
      :type => "text/csv; charset=utf8; header=present",
      :disposition => "attachment; filename=incomes.csv"
    )
  end

  def edit
    @sources = current_user.sources.map { |c| [ c.name, c.id ] }
    render :layout => nil
  end

  def index
    @incomes = current_user.incomes.order("date DESC, created_at DESC").limit(12)
    render :layout => nil
  end

  def new
    @sources = current_user.sources.map { |c| [ c.name, c.id ] }
    render :layout => nil
  end

  def show
    @income = Income.find(params[:id])
    render :json => @income
  end

  def update
    @income = Income.find(params[:id])

    if @income.update_attributes(params[:income])
       render :json => @income, :status => 200
    else
      render :json => "this should be an error message", :status => 500
    end
  end

end
