class NgViewsController < ApplicationController
  skip_before_filter :require_authentication, :only => [:show]

  def show
  end

end
