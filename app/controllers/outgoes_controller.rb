class OutgoesController < ApplicationController
  before_filter :format_data, :only => [:create, :update]
  respond_to :js, :only => [:create, :show, :update]

  def by_month
    date = params[:date] || Date.today
    @outgoes = Outgo.by_month(date).order("date DESC, created_at DESC")

    render :action => :index, :layout => nil
  end

  def create
    @outgo = current_user.outgoes.build(params[:outgo])

    if @outgo.save
      render :json => @outgo, :status => 201
    else
      render :json => "this should be an error message", :status => 500
    end
  end

  def csv
    @outgoes_csv = current_user.outgoes.scoped.to_csv

    send_data(
      @outgoes_csv,
      :type => "text/csv; charset=utf8; header=present",
      :disposition => "attachment; filename=outgoes.csv"
    )
  end

  def edit
    @categories = current_user.categories.map { |c| [ c.name, c.id ] }
    @tags       = current_user.tags.map(&:name)

    render :layout => nil
  end

  def index
    @outgoes = current_user.outgoes.order("date DESC, created_at DESC").limit(12)
    render :layout => nil
  end

  def new
    @categories = current_user.categories.map { |c| [ c.name, c.id ] }
    @tags       = current_user.tags.map(&:name)

    render :layout => nil
  end

  def show
    @outgo = Outgo.find(params[:id])
    render :json => @outgo, :serializer => OutgoSerializer
  end

  def update
    @outgo = Outgo.find(params[:id])

    if @outgo.update_attributes(params[:outgo])
      render :json => @outgo, :status => 200
    else
      render :json => "this should be an error message", :status => 500
    end
  end

  private

  def format_data
    params[:outgo][:tags] = if params[:outgo][:tags].nil?
      []
    else
      params[:outgo][:tags].map { |name| current_user.tags.find_or_create_by_name name }
    end
  end

end
