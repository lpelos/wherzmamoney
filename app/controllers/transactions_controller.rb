class TransactionsController < ApplicationController
  before_action :require_authentication
  before_action :format_data, :only => [:index, :year_extract]

  def index
    respond_to do |format|

      format.html { render :layout => nil }

      format.json do
        if params.except(:controller, :action, :format, :type).empty?
          @transactions = current_user.transactions.by_type(params[:type]).limit(15).order("date DESC, created_at DESC")
        else
          @transactions = current_user.transactions.filtered_by(params).order("date DESC, created_at DESC")
        end

        grouped_by_type = @transactions.group_by(&:type)
        @income = calculate_total_value(grouped_by_type["Income"])
        @outgo  = calculate_total_value(grouped_by_type["Outgo"])

        @title = extract_title(params)

        render :json => @transactions, :each_serializer => TransactionSerializer, :meta => { :income => @income, :outgo => @outgo, :title => @title }
      end
    end
  end

  def year_extract
    respond_to do |format|
      format.html { render :layout => nil }

      format.json do
        @values_by_month = transactions_by_year params[:year]

        incomes_by_month = @values_by_month.map{|v| { :value => v[:values][:income] } }
        outgoes_by_month = @values_by_month.map{|v| { :value => v[:values][:outgo] } }

        @income = calculate_total_value(incomes_by_month, true)
        @outgo  = calculate_total_value(outgoes_by_month, true)

        @title = extract_title(params)

        render :json => @values_by_month, :meta => { :income => @income, :outgo => @outgo, :title => @title }
      end
    end
  end

  def months_modal
    render :layout => nil
  end

  def search
    @categories = current_user.categories.map { |c| [ c.name, c.id ] }
    @sources    = current_user.sources.map { |s| [ s.name, s.id ] }
    @tags       = current_user.tags.map(&:name)

    render :layout => nil
  end

  def years_modal
    render :layout => nil
  end

  private

  def calculate_total_value(array_of_values = [], is_hash = false)
    total_value = 0

    unless array_of_values.nil?
      if is_hash
        array_of_values.each{ |t| total_value += t[:value] }
      else
        array_of_values.each{ |t| total_value += t.value }
      end
    end

    total_value
  end

  def extract_title(params)
    title = [I18n.t("extract")]
    if params[:year].present?
      if params[:month].present?
        title << I18n.l(Date.new(params[:year], params[:month]), :format => :short_year_month)
      else
        title << params[:year]
      end
    end

    title.join(" - ")
  end

  def format_data
    params[:month] = params[:month].to_i if !!params[:month]
    params[:year]  = params[:year].to_i  if !!params[:year]
    params[:type]  = params[:type].capitalize.singularize if !!params[:type]
  end

  def transactions_by_year(year = Date.today.year)
    date = Date.new(year)
    transactions_by_month = current_user.transactions.by_year(date).order("date").group_by{ |t| t.date.beginning_of_month }

    @totals_by_month = []
    transactions_by_month.each do |first_day_of_month, transactions_in_month|
      month = first_day_of_month.month

      grouped_by_type = transactions_in_month.group_by(&:type)
      income = calculate_total_value(grouped_by_type["Income"])
      outgo  = calculate_total_value(grouped_by_type["Outgo"])

      @totals_by_month.push :month => month, :values => { :income => income, :outgo => outgo }
    end

    @totals_by_month
  end

end
