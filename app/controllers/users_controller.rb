# encoding: UTF-8
class UsersController < ApplicationController
  skip_before_filter :require_authentication, :only => [:create, :login, :welcome]

  def update
    # Código abaixo não funciona
    if User.authenticate(current_user.email, params[:current_password])
      if @user = current_user.update_attributes(params[:user])
        #se salvar
      else
        #se não salvar
      end
    else
      #se "senha atual" nao corresponder
      #retornar erros (em json?)
    end
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      redirect_to root_url, :notice => "Cadastrado"
    else
      render :text => "welcome"
    end
  end

  def login
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to root_url
    else
      # TODO
      # Redirecionamento para URL passada em parâmetros não está runcionando
      redirect_to params[:redirect_url] || welcome_path
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to welcome_path
  end

  def welcome
    @new_user = User.new
    @user = current_user

    render :layout => nil
  end
end
