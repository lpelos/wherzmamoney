class Income < Transaction

  attr_accessible :date, :description, :source_id, :user_id, :value

  belongs_to :user
  belongs_to :source

  def self.import(file, user)
    CSV.foreach(file.path, :headers => true) do |row|

      # Convert row to hash with symbol keys
      attr_hash = {}
      row.to_hash.each_pair do |k,v|
        attr_hash.merge!({k.downcase.to_sym => v})
      end

      # Find of create source by name
      # return source id
      source_id = if attr_hash.has_key?(:source)
        Source.find_or_create_by_name(:name => attr_hash[:source], :user_id => user.id).id
      else
        nil
      end

      # Exclude invalid keys and values
      attr_hash.reverse_merge!(
        :date => Date.today,
        :value => 0,
        :description => "",
      )

      # Exclude invalid keys and values
      keepers = column_names.map(&:to_sym)
      attr_hash.keep_if { |k,_| keepers.include? k }

      # Applies associations
      attr_hash.merge!(
        :source_id => source_id,
        :user_id => user.id
      )

      create! attr_hash
    end
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["Date", "Value", "Source", "Description"]

      scoped.each do |i|
        csv << [i.date.to_s, i.value.to_s, i.source.name, i.description]
      end
    end
  end

end
