class Outgo < Transaction

  attr_accessible :category_id, :date, :description, :tags, :user_id, :value

  belongs_to :category
  belongs_to :user

  has_and_belongs_to_many :tags, :join_table => 'outgoes_tags', :foreign_key => 'outgo_id'

  def self.import(file, user)
    CSV.foreach(file.path, :headers => true) do |row|

      # convert row to hash with symbol keys
      attr_hash = {}
      row.to_hash.each_pair do |k,v|
        attr_hash.merge!({k.downcase.to_sym => v})
      end

      # Find of create category by name
      # return its id
      category_id = if attr_hash.has_key?(:category)
        Category.find_or_create_by_name(:name => attr_hash[:category], :user_id => user.id).id
      else
        nil
      end

      # Find or create tags by name
      # return array with tags' names
      tags_array = if attr_hash.has_key?(:tags)
        attr_hash[:tags].split(/;\s?|,\s?/).map do |tag_name|
          user.tags.find_or_create_by_name(:name => tag_name)
        end
      else
        []
      end

      # Include default values if they don't already exist
      attr_hash.reverse_merge!(
        :date => Date.today,
        :value => 0,
        :description => "",
      )

      # Exclude invalid keys and values
      keepers = column_names.map(&:to_sym)
      attr_hash.keep_if { |k,_| keepers.include? k }

      # Applies associations
      attr_hash.merge!(
        :category_id => category_id,
        :tags        => tags_array,
        :user_id     => user.id
      )

      create! attr_hash
    end
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["Date", "Value", "Category", "Tags", "Description"]

      scoped.each do |o|
        o_values = []
        o_values.push o.date.to_s
        o_values.push o.value.to_s
        o_values.push o.category.name
        o_values.push o.tags.any? ? o.tags.map(&:name).join("; ") : ""
        o_values.push o.description

        csv << o_values
      end
    end
  end

end
