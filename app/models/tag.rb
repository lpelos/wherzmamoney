class Tag < ActiveRecord::Base

  attr_accessible :name, :user_id
  belongs_to :user
  has_and_belongs_to_many :outgoes, :join_table => 'outgoes_tags', :foreign_key => 'tags_id'

  def self.import(file, user)
    CSV.foreach(file.path, :headers => true) do |row|

      attr_hash = {}
      row.to_hash.each_pair do |k,v|
        attr_hash.merge!({k.downcase.to_sym => v})
      end

      attr_hash.reverse_merge!(
        :name => "",
        :user_id => user.id
      )

      keepers = column_names.map(&:to_sym)
      attr_hash.keep_if { |k,_| keepers.include? k }

      find_or_create_by_name(attr_hash)
    end
  end

end
