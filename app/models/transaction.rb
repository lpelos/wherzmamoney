class Transaction < ActiveRecord::Base
  attr_accessible :type, :value, :date, :source_id, :category_id, :tag_names, :description, :user_id

  belongs_to :user
  belongs_to :category, :foreign_key => 'outgo_id'
  belongs_to :source,   :foreign_key => 'income_id'

  scope :income, where(type: "Income")
  scope :outgo, where(type: "Outgo")

  def self.by_month(month, year = Date.today.year)
    if month
      date = Date.new(year, month)
      where(date: date.beginning_of_month..date.end_of_month)
    else
      scoped
    end
  end

  def self.by_range(date_from, date_to)
    if date_from || date_to
      if date_to.nil?
        where("date > #{date_from}")
      elsif date_from.nil?
        where("date < #{date_to}")
      else
        where(date: date_from..date_to)
      end
    else
      scoped
    end
  end

  def self.by_type(type)
    if type == "Income" || type == "Outgo"
      where(type: type)
    else
      scoped
    end
  end

  def self.by_value_range(value_from, value_to)
    if value_from || value_to
      if value_to.nil?
        where("value > #{value_from}")
      elsif value_from.nil?
        where("value < #{value_to}")
      else
        where(value: value_from..value_to)
      end
    else
      scoped
    end
  end

  def self.by_year(date)
    where(date: date.beginning_of_year..date.end_of_year)
  end

  def self.filtered_by(p = {})
    conditions = {}

    conditions[:type] = p[:type]               if p[:type] && (p[:type] == "Outgo" || p[:type] == "Income")
    conditions[:value] = p[:value]             if p[:value]
    conditions[:date] = p[:date]               if p[:date]
    conditions[:source_id] = p[:source_id]     if p[:source_id]
    conditions[:category_id] = p[:category_id] if p[:category_id]
    conditions[:source_id] = p[:sources]       if p[:sources]    && !p[:sources].empty?
    conditions[:category_id] = p[:categories]  if p[:categories] && !p[:categories].empty?
    conditions[:tag_id] = p[:tags]             if p[:tags]       && !p[:tags].empty?

    if p[:month]
      date = Date.new(p[:year] || Date.today.year, p[:month])
      conditions[:date] = date.beginning_of_month..date.end_of_month
    end

    # if p[:date_from] || p[:date_to]
    #   if p[:date_to].nil?
    #     conditions.and "date > #{p[:date_from]}"
    #   elsif p[:date_from].nil?
    #     conditions.and "date < #{p[:date_to]}"
    #   else
    #     conditions.and date: p[:date_from]..p[:date_to]
    #   end
    # end

    conditions[:date] = p[:date_from]..p[:date_to] if p[:date_from] && p[:date_to]

    # if p[:value_from] || p[:value_to]
    #   if p[:value_to].nil?
    #     conditions.and "value > #{p[:value_from]}"
    #   elsif p[:value_from].nil?
    #     conditions.and "value < #{p[:value_to]}"
    #   else
    #     conditions.and value: p[:value_from]..p[:value_to]
    #   end
    # end

    conditions[:value] = p[:value_from]..p[:value_to] if p[:value_from] && p[:value_to]

    scoped :conditions => conditions
  end

  def tag_names
    tags.map(&:name)
  end

  def tag_names=(tags)
    self.tags = tags.map do |name|
      user.tags.find_or_initialize_by_name name
    end
  end

  def self.to_csv_file(options = {})
    CSV.generate(options) do |csv|
      csv << ["Date", "Value", "Type", "Source", "Category", "Tags", "Description"]

      all.each do |transaction|
        transaction_values = []
        transaction_values.push transaction.date.to_s
        transaction_values.push transaction.value.to_s
        transaction_values.push transaction.type
        transaction_values.push transaction.source ? transaction.source.name : ""
        transaction_values.push transaction.category ? transaction.category.name : ""
        transaction_values.push transaction.tags.any? ? transaction.tags.map(&:name).join("; ") : ""
        transaction_values.push transaction.description

        csv << transaction_values
      end
    end
  end

end
