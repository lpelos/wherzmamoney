class User < ActiveRecord::Base

  attr_accessible :email, :name, :password, :password_confirmation

  attr_accessor :password
  before_save :encrypt_password

  has_many :categories
  has_many :incomes
  has_many :outgoes
  has_many :sources
  has_many :tags
  has_many :transactions

  validates :password, :presence => true, :confirmation => true
  validates :email, :presence => true, :uniqueness => true

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_digest == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_digest = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

end
