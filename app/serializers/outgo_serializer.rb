class OutgoSerializer < ActiveModel::Serializer
  self.root = false

  def tags
    object.tag_names
  end

  attributes :id, :category_id, :date, :description, :tags, :value
end
