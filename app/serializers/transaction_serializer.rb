class TransactionSerializer < ActiveModel::Serializer

  def category
    object.category
  end

  def tags
    object.type == "Outgo" ? object.tag_names : nil
  end

  def source
    object.source
  end

  attributes :id, :category, :date, :description, :source, :tags, :value
end
