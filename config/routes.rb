Wherzmamoney::Application.routes.draw do

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    post 'login'   => 'users#login',   :as => 'login'
    get  'logout'  => 'users#logout',  :as => 'logout'
    get  'welcome' => 'users#welcome', :as => 'welcome'

    # Incomes
    get 'incomes/csv'  => 'incomes#csv'
    get 'incomes/edit' => 'incomes#edit'

    # Outgoes
    get 'outgoes/csv'  => 'outgoes#csv'
    get 'outgoes/edit' => 'outgoes#edit'

    # Transactions
    get 'transactions/by_year'         => 'transactions#year_extract'
    get 'transactions/by_year/:year'   => 'transactions#year_extract'
    get 'transactions/modals/by-month' => 'transactions#months_modal'
    get 'transactions/modals/by-year'  => 'transactions#years_modal'
    get 'transactions/search'          => 'transactions#search'

    # Resources
    resources :import_export, :only => [:create, :index, :show]
    resources :incomes
    resources :ng_views, :only => :show
    resources :outgoes
    resources :users
    resources :transactions, :only => :index

    root :to => 'ng_views#show'
  end

  match '*path', via: [:get, :post], to: redirect("/#{I18n.default_locale}/%{path}")
  match '', via: [:get, :post], to: redirect("/#{I18n.default_locale}/")

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
